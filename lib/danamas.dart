library danamas;

import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mime/mime.dart';
import 'package:uuid/uuid.dart' as _puuid;
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

part 'stores.dart';
part 'streams.dart';
part 'models.dart';
part 'utils/utils.dart';
part 'utils/verifyemail.dart';

part 'pages/pinjaman.dart';
part 'pages/danamaspage.dart';
part 'pages/registrasisatu.dart';
part 'pages/registrasidua.dart';
part 'pages/registrasiphoto.dart';
part 'pages/registrasitiga.dart';
part 'pages/registrasiempat.dart';
part 'pages/historypage.dart';
part 'pages/pengajuan.dart';

part 'assets/initasset.dart';
part 'assets/colors.dart';
part 'assets/needregister.dart';
part 'assets/stepsatu.dart';
part 'assets/stepdua.dart';
part 'assets/steptiga.dart';
part 'assets/stepempat.dart';
part 'assets/alertwarning.dart';
part 'assets/noktp.dart';
part 'assets/successregister.dart';
part 'assets/noloan.dart';
part 'assets/uangpinjaman.dart';
part 'assets/jatuhtempo.dart';
part 'assets/denda.dart';
part 'assets/bank.dart';
part 'assets/map.dart';
part 'assets/failedregister.dart';
part 'assets/iconwarning.dart';
part 'assets/iconcheck.dart';
part 'assets/iconcopy.dart';
part 'assets/profilepicture.dart';
part 'assets/location.dart';

part 'repo/api.dart';
part 'repo/repositories.dart';

part 'widgets/buttons.dart';
part 'widgets/textfield.dart';
part 'widgets/customalert.dart';
part 'widgets/alertdialog.dart';
part 'widgets/appbar.dart';
part 'widgets/pinjamanaktif.dart';
part 'widgets/historypinjaman.dart';
