part of danamas;

PreferredSizeWidget _AppBar(
    {@required BuildContext context,
    @required String title,
    @required String subtitle,
    @required Uint8List imgstep}) {
  return AppBar(
    automaticallyImplyLeading: false,
    toolbarHeight: 80,
    centerTitle: false,
    backgroundColor: _primaryRed,
    title: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        InkResponse(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back,
            size: 20,
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(fontFamily: 'VarelaRound', fontSize: 18),
            ),
            Text(
              subtitle,
              style: TextStyle(fontFamily: 'VarelaRound', fontSize: 12),
            ),
          ],
        ),
      ],
    ),
    actions: [
      Container(
          constraints: BoxConstraints(maxWidth: 40, maxHeight: 40),
          child: Image.memory(imgstep, fit: BoxFit.contain)),
      SizedBox(
        width: 20,
      )
    ],
  );
}
