part of danamas;

class _SingleButton extends StatelessWidget {
  final String label;
  final Function onTap;
  _SingleButton({@required this.label, @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return _TheButton(
        child: Center(
          child: Text(
            label,
            style: TextStyle(
                color: Colors.white, fontFamily: 'VarelaRound', fontSize: 14),
          ),
        ),
        label: label,
        onTap: onTap);
  }
}

class _WithArrowButton extends StatelessWidget {
  final String label;
  final Function onTap;
  final IconData icon;
  _WithArrowButton({@required this.label, @required this.onTap, this.icon});
  @override
  Widget build(BuildContext context) {
    return _TheButton(
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(
            label,
            style: TextStyle(
                color: Colors.white, fontFamily: 'VarelaRound', fontSize: 14),
          ),
          Icon(
            icon ?? Icons.arrow_forward,
            color: Colors.white,
            size: 20,
          )
        ]),
        label: label,
        onTap: onTap);
  }
}

class _TheButton extends StatelessWidget {
  final String label;
  final Function onTap;
  final Widget child;
  _TheButton(
      {@required this.label, @required this.onTap, @required this.child});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: InkResponse(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  color: _primaryRed),
              child: child,
            ),
          ),
        )
      ],
    );
  }
}

class _BackButton extends StatelessWidget {
  final Function onTap;
  final Widget child;
  _BackButton({@required this.onTap, @required this.child});
  @override
  Widget build(BuildContext context) {
    return InkResponse(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
          decoration: BoxDecoration(
              border: Border.all(color: _boldGrey),
              borderRadius: BorderRadius.all(Radius.circular(6)),
              color: Colors.white),
          child: child,
        ));
  }
}

class _OutLineBtn extends StatelessWidget {
  final String label;
  final Function onTap;
  final EdgeInsets margin;

  _OutLineBtn(
      {@required this.label, @required this.onTap, @required this.margin});

  @override
  Widget build(BuildContext context) {
    return InkResponse(
      onTap: onTap,
      child: Container(
          alignment: Alignment.center,
          margin: margin,
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: _primaryRed)),
          child: Text(label,
              style: TextStyle(
                  color: _primaryRed,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'VarelaRound',
                  fontSize: 13))),
    );
  }
}
