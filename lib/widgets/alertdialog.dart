part of danamas;

void _closeLoading(context) {
  if (_isLoading < 0) return;
  Navigator.of(context).pop();
  _isLoading = -1;
}

void _autoCloseLoading(context) async {
  await Future.delayed(Duration(minutes: 3));
  if (_isLoading > 0) {
    final _expired = _unixtime() - 15;
    if (_isLoading < _expired) _closeLoading(context);
  }
}

void _showLoadingDialog(context, {text = ""}) {
  if (_isLoading > 0) return;
  _isLoading = _unixtime();
  _autoCloseLoading(context);
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => WillPopScope(
            onWillPop: () async => false,
            child: CustomAlertDialog(
              contentPadding: EdgeInsets.all(20.0),
              content: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 20.0),
                  Text(
                    (text == "") ? "Loading..." : text,
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  )
                ],
              ),
            ),
          ));
}

Future<String> _alertWarning(
    BuildContext context, String teks, String label) async {
  return await showDialog(
      context: context,
      builder: (_) => CustomAlertDialog(
            contentPadding: EdgeInsets.all(20.0),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.memory(
                  _imgalertwarning,
                  width: 112,
                  height: 112,
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  teks,
                  style: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'VarelaRound'),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 18,
                ),
                _SingleButton(
                  label: label,
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ));
}
