part of danamas;

class _TextField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;
  final bool obscureText;
  final bool readOnly;
  final Widget suffix;
  final TextInputType keyboardType;
  _TextField(
      {@required this.labelText,
      @required this.controller,
      this.obscureText,
      this.readOnly,
      this.suffix,
      this.keyboardType});
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: new ThemeData(primaryColor: _primaryRed),
      child: TextField(
        keyboardType: keyboardType ?? TextInputType.text,
        readOnly: readOnly == null ? false : readOnly,
        obscureText: obscureText == null ? false : obscureText,
        controller: controller,
        cursorColor: _primaryRed,
        decoration: InputDecoration(
            labelText: labelText,
            labelStyle: TextStyle(fontSize: 14, color: _boldGrey)),
      ),
    );
  }
}

class _SelectField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;
  final List<_PresetModel> listdata;
  final Function onChanged;
  final String value;
  _SelectField(
      {@required this.labelText,
      @required this.controller,
      @required this.listdata,
      @required this.onChanged,
      this.value});
  @override
  Widget build(BuildContext context) {
    if (value != null && value != '') {
      final _curval =
          listdata.where((element) => element.code == value).toList();
      if (_curval.length > 0) {
        controller.text = _curval[0].description;
      }
    }
    return Stack(
      children: [
        _TextField(
          labelText: labelText,
          controller: controller,
          readOnly: true,
        ),
        Positioned(
            right: 10,
            bottom: 0,
            child: DropdownButton(
              underline: Container(),
              icon: Icon(Icons.keyboard_arrow_down),
              items: listdata.map((e) {
                return new DropdownMenuItem<_PresetModel>(
                  child: new Text(e.description ?? '---'),
                  value: e,
                );
              }).toList(),
              onChanged: (_PresetModel val) {
                controller.text = val.description;
                onChanged(val.code);
              },
            )),
      ],
    );
  }
}
