part of danamas;

class _HPlistWidget extends StatefulWidget {
  final _PinjamanModel data;
  final bool isOpen;
  _HPlistWidget(this.data, {this.isOpen});
  @override
  _HPlistWidgetState createState() => _HPlistWidgetState();
}

class _HPlistWidgetState extends State<_HPlistWidget> {
  bool _isOpen;
  @override
  void initState() {
    _isOpen = widget.isOpen ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Expanded(
                  child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      width: 6,
                      height: 6,
                      margin: const EdgeInsets.only(right: 20),
                      decoration: BoxDecoration(
                          color: _primaryRed,
                          borderRadius: BorderRadius.all(
                            Radius.circular(3),
                          ))),
                  Text(
                    _dateformat1(widget.data.createdAt),
                    style: TextStyle(
                        fontFamily: 'VarelaRound',
                        fontSize: 13,
                        color: _primaryRed),
                  )
                ],
              )),
              _isOpen
                  ? InkResponse(
                      onTap: () {
                        setState(() {
                          _isOpen = !_isOpen;
                        });
                      },
                      child: Icon(Icons.keyboard_arrow_up, color: _primaryRed))
                  : InkResponse(
                      onTap: () {
                        setState(() {
                          _isOpen = !_isOpen;
                        });
                      },
                      child: Icon(Icons.keyboard_arrow_down))
            ],
          ),
          _isOpen
              ? Container(
                  margin: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: _lightGrey,
                  ),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Container(
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Color(0xFFE7E7E7)))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Container(
                                  width: 30,
                                  height: 30,
                                  margin: const EdgeInsets.only(right: 20),
                                  child: Image.memory(_imguangpinjaman)),
                              Expanded(
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                    Text(
                                      'Jumlah Pinjaman',
                                      style: TextStyle(
                                          fontFamily: 'VarelaRound',
                                          fontSize: 11,
                                          color: _boldGrey),
                                    ),
                                    SizedBox(height: 6),
                                    Text(
                                      _moneyFormat(widget.data.totalPinjaman),
                                      style: TextStyle(
                                          fontFamily: 'VarelaRound',
                                          fontSize: 15,
                                          color: Colors.black),
                                    )
                                  ]))
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 20),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFEE93),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Container(
                                    width: 18,
                                    height: 18,
                                    margin: const EdgeInsets.only(right: 10),
                                    child: Image.memory(_iconcheck)),
                                Expanded(
                                    child: Text(
                                        'Tanggal Pelunasan: ${_dateformat1(widget.data.pelunasan)}',
                                        style: TextStyle(
                                            fontFamily: 'VarelaRound',
                                            fontSize: 13,
                                            color: Colors.black)))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    // Container(
                    //     padding: const EdgeInsets.all(20),
                    //     child:
                    //         Column(mainAxisSize: MainAxisSize.min, children: [
                    //       Row(
                    //         children: [
                    //           Container(
                    //               width: 30,
                    //               height: 30,
                    //               margin: const EdgeInsets.only(right: 20),
                    //               child: Image.memory(_imgjatuhtempo)),
                    //           Expanded(
                    //               child: Column(
                    //                   mainAxisSize: MainAxisSize.min,
                    //                   crossAxisAlignment:
                    //                       CrossAxisAlignment.start,
                    //                   children: [
                    //                 Text(
                    //                   'Denda',
                    //                   style: TextStyle(
                    //                       fontFamily: 'VarelaRound',
                    //                       fontSize: 11,
                    //                       color: _boldGrey),
                    //                 ),
                    //                 SizedBox(height: 6),
                    //                 Text(
                    //                   _moneyFormat(widget.data.denda),
                    //                   style: TextStyle(
                    //                       fontFamily: 'VarelaRound',
                    //                       fontSize: 15,
                    //                       color: Colors.black),
                    //                 )
                    //               ]))
                    //         ],
                    //       ),
                    //   Container(
                    //     margin: const EdgeInsets.only(top: 20),
                    //     padding: const EdgeInsets.all(10),
                    //     decoration: BoxDecoration(
                    //         color: Color(0xFFFFEE93),
                    //         borderRadius:
                    //             BorderRadius.all(Radius.circular(8))),
                    //     child: Row(
                    //       children: [
                    //         Container(
                    //             width: 18,
                    //             height: 18,
                    //             margin: const EdgeInsets.only(right: 10),
                    //             child: Image.memory(_iconcheck)),
                    //         Expanded(
                    //             child: Text(
                    //                 'Tanggal Pelunasan: ${_dateformat1(widget.data.pelunasan)}',
                    //                 style: TextStyle(
                    //                     fontFamily: 'VarelaRound',
                    //                     fontSize: 13,
                    //                     color: Colors.black)))
                    //       ],
                    //     ),
                    //   )
                    // ]))
                  ]))
              : Container()
        ],
      ),
    );
  }
}

class _HistoryPinjaman extends StatefulWidget {
  @override
  _HistoryPinjamanState createState() => _HistoryPinjamanState();
}

class _HistoryPinjamanState extends State<_HistoryPinjaman> {
  ScrollController _scrollController = new ScrollController();
  List<Map<String, dynamic>> data = [];
  bool _showMore = true;
  int _page = 1;
  int _limit = 10;

  void loading() async {
    await Future.delayed(Duration(milliseconds: 50));
    _showLoadingDialog(context);
  }

  void loadData() async {
    loading();
    final _respon = await _getHistory();
    await Future.delayed(Duration(milliseconds: 50));
    _closeLoading(context);
    if (_respon.code == 200 && _respon.status == true) {
      final _curdat = _respon.data as List<dynamic>;
      if (_curdat.length < _limit || _curdat.length == 0)
        _showMore = false;
      else
        _showMore = true;
      if (_curdat.length > 0) {
        data = [];
        for (int i = 0; i < _curdat.length; i++) {
          final _curdatcd = _curdat[i] as Map<String, dynamic>;
          data.add(_curdatcd);
        }
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (data.length > 0) {
      List<Widget> _list = [];
      for (int i = 0; i < data.length; i++) {
        final _curdat = _PinjamanModel.fromJson(data[i]);
        final _isOpen = i == 0 ? true : null;
        _list.add(_HPlistWidget(
          _curdat,
          isOpen: _isOpen,
        ));
      }
      return Container(
        padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: _list,
                ),
              ),
            ),
            _showMore == true
                ? Container(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: _WithArrowButton(
                        label: 'Tampilkan Lebih Banyak',
                        icon: Icons.keyboard_arrow_down,
                        onTap: () {
                          _page = _page + 1;
                          loadData();
                          _scrollController.animateTo(
                            _scrollController.offset + 200,
                            curve: Curves.easeOut,
                            duration: const Duration(seconds: 1),
                          );
                        }))
                : Container()
          ],
        ),
      );
    }

    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                constraints: BoxConstraints(maxWidth: 150),
                child: Image.memory(
                  _imgnoloan,
                  fit: BoxFit.fitWidth,
                )),
            SizedBox(
              height: 20,
            ),
            Text(
              'Data tidak ditemukan.',
              style: TextStyle(
                  height: 1.3,
                  fontSize: 15,
                  fontFamily: 'Helvetica',
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
