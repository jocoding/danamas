part of danamas;

class _PinjamanAktif extends StatefulWidget {
  @override
  _PinjamanAktifState createState() => _PinjamanAktifState();
}

class _PinjamanAktifState extends State<_PinjamanAktif> {
  Map<String, dynamic> data;
  bool isDispose = false;
  String _message = 'Anda tidak mempunyai pinjaman aktif.';

  void loading() async {
    await Future.delayed(Duration(milliseconds: 50));
    _showLoadingDialog(context);
  }

  void loadData() async {
    loading();
    final _ractiveloan = await _getActiveloan();
    if (_ractiveloan.code == 200 && _ractiveloan.status == true) {
      await Future.delayed(Duration(milliseconds: 50));
      _closeLoading(context);
      if (isDispose == false)
        setState(() {
          data = _ractiveloan.data;
        });
    } else if (_ractiveloan.message is String && _ractiveloan.message != '') {
      _closeLoading(context);
      setState(() {
        _message = _ractiveloan.message;
      });
    }
  }

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  void dispose() {
    isDispose = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _logging(data);
    if (data != null) {
      final _pinjaman = _PinjamanModel.fromJson(data);
      if (_pinjaman.isPaid == false) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: _lightGrey,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Color(0xFFE7E7E7)))),
                    child: Row(
                      children: [
                        Container(
                            width: 30,
                            height: 30,
                            margin: const EdgeInsets.only(right: 20),
                            child: Image.memory(_imguangpinjaman)),
                        Expanded(
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                              Text(
                                'Jumlah Pinjaman',
                                style: TextStyle(
                                    fontFamily: 'VarelaRound',
                                    fontSize: 11,
                                    color: _boldGrey),
                              ),
                              SizedBox(height: 6),
                              Text(
                                _moneyFormat(_pinjaman.totalPinjaman),
                                style: TextStyle(
                                    fontFamily: 'VarelaRound',
                                    fontSize: 15,
                                    color: Colors.black),
                              )
                            ]))
                      ],
                    ),
                  ),
                  _pinjaman.isDisbursement == false
                      ? Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: Color(0xFFE7E7E7)))),
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFEE93),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Container(
                                    width: 18,
                                    height: 18,
                                    margin: const EdgeInsets.only(right: 10),
                                    child: Image.memory(_iconcheck)),
                                Expanded(
                                    child: Text('Pinjaman sedang dalam proses.',
                                        style: TextStyle(
                                            fontFamily: 'VarelaRound',
                                            fontSize: 13,
                                            color: Colors.black)))
                              ],
                            ),
                          ))
                      : Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: Color(0xFFE7E7E7)))),
                          child:
                              Column(mainAxisSize: MainAxisSize.min, children: [
                            Row(
                              children: [
                                Container(
                                    width: 30,
                                    height: 30,
                                    margin: const EdgeInsets.only(right: 20),
                                    child: Image.memory(_imgjatuhtempo)),
                                Expanded(
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                      Text(
                                        'Jatuh Tempo Pinjaman',
                                        style: TextStyle(
                                            fontFamily: 'VarelaRound',
                                            fontSize: 11,
                                            color: _boldGrey),
                                      ),
                                      SizedBox(height: 6),
                                      _pinjaman.jatuhTempo == null
                                          ? Container()
                                          : Text(
                                              _dateformat1(
                                                  _pinjaman.jatuhTempo),
                                              style: TextStyle(
                                                  fontFamily: 'VarelaRound',
                                                  fontSize: 15,
                                                  color: Colors.black),
                                            )
                                    ]))
                              ],
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Color(0xFFEEEEEE),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: Row(
                                children: [
                                  Container(
                                      width: 12,
                                      height: 12,
                                      margin: const EdgeInsets.only(right: 10),
                                      child: Image.memory(_iconwarning)),
                                  Expanded(
                                      child: Text(
                                          'Bayar sebelum jatuh tempo untuk menghindari denda.',
                                          style: TextStyle(
                                              fontFamily: 'VarelaRound',
                                              fontSize: 9,
                                              color: _boldGrey)))
                                ],
                              ),
                            )
                          ])),
                  Container(
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Color(0xFFE7E7E7)))),
                    child: Row(
                      children: [
                        Container(
                            width: 30,
                            height: 30,
                            margin: const EdgeInsets.only(right: 20),
                            child: Image.memory(_imgdenda)),
                        Expanded(
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                              Text(
                                'Kode Pinjaman',
                                style: TextStyle(
                                    fontFamily: 'VarelaRound',
                                    fontSize: 11,
                                    color: _boldGrey),
                              ),
                              SizedBox(height: 6),
                              InkResponse(
                                onTap: () async {
                                  await Clipboard.setData(
                                      ClipboardData(text: _pinjaman.loanCode));
                                  final _copied =
                                      await Clipboard.getData('text/plain');
                                  if (_copied.text == _pinjaman.loanCode) {
                                    Fluttertoast.showToast(
                                        msg:
                                            "${_pinjaman.loanCode} berhasil disalin",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        backgroundColor: Colors.grey,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  }
                                },
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        _pinjaman.loanCode,
                                        style: TextStyle(
                                            fontFamily: 'VarelaRound',
                                            fontSize: 15,
                                            color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                        margin: const EdgeInsets.only(left: 10),
                                        width: 17,
                                        height: 17,
                                        child: Image.memory(_iconcopy))
                                  ],
                                ),
                              )
                            ]))
                      ],
                    ),
                  ),
                  // _pinjaman.isDisbursement == false
                  //     ? Container()
                  //     : Container(
                  //         padding: const EdgeInsets.all(20),
                  //         decoration: BoxDecoration(
                  //             border: Border(
                  //                 bottom:
                  //                     BorderSide(color: Color(0xFFE7E7E7)))),
                  //         child: Row(
                  //           children: [
                  //             Container(
                  //                 width: 30,
                  //                 height: 30,
                  //                 margin: const EdgeInsets.only(right: 20),
                  //                 child: Image.memory(_imgdenda)),
                  //             Expanded(
                  //                 child: Column(
                  //                     mainAxisSize: MainAxisSize.min,
                  //                     crossAxisAlignment:
                  //                         CrossAxisAlignment.start,
                  //                     children: [
                  //                   Text(
                  //                     'Jumlah Denda',
                  //                     style: TextStyle(
                  //                         fontFamily: 'VarelaRound',
                  //                         fontSize: 11,
                  //                         color: _boldGrey),
                  //                   ),
                  //                   SizedBox(height: 6),
                  //                   Text(
                  //                     _moneyFormat(_pinjaman.denda),
                  //                     style: TextStyle(
                  //                         fontFamily: 'VarelaRound',
                  //                         fontSize: 15,
                  //                         color: Colors.black),
                  //                   )
                  //                 ]))
                  //           ],
                  //         ),
                  //       ),
                  _pinjaman.isDisbursement == false
                      ? Container()
                      : Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: Color(0xFFE7E7E7)))),
                          child: Row(
                            children: [
                              Container(
                                  width: 30,
                                  height: 30,
                                  margin: const EdgeInsets.only(right: 20),
                                  child: Image.memory(_imgbank)),
                              Expanded(
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                    Text(
                                      'Informasi Pembayaran Pinjaman',
                                      style: TextStyle(
                                          fontFamily: 'VarelaRound',
                                          fontSize: 11,
                                          color: _boldGrey),
                                    ),
                                    SizedBox(height: 6),
                                    Text(
                                      _pinjaman.namaBank,
                                      style: TextStyle(
                                          fontFamily: 'VarelaRound',
                                          fontSize: 15,
                                          color: _primaryRed),
                                    ),
                                    SizedBox(height: 6),
                                    InkResponse(
                                      onTap: () async {
                                        await Clipboard.setData(ClipboardData(
                                            text: _pinjaman.nomorRekening));
                                        final _copied = await Clipboard.getData(
                                            'text/plain');
                                        if (_copied.text ==
                                            _pinjaman.nomorRekening) {
                                          Fluttertoast.showToast(
                                              msg:
                                                  "${_pinjaman.nomorRekening} berhasil disalin",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor: Colors.grey,
                                              textColor: Colors.white,
                                              fontSize: 16.0);
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              _pinjaman.nomorRekening,
                                              style: TextStyle(
                                                  fontFamily: 'VarelaRound',
                                                  fontSize: 15,
                                                  color: Colors.black),
                                            ),
                                          ),
                                          Container(
                                              margin: const EdgeInsets.only(
                                                  left: 10),
                                              width: 17,
                                              height: 17,
                                              child: Image.memory(_iconcopy))
                                        ],
                                      ),
                                    )
                                  ]))
                            ],
                          ),
                        ),
                ],
              ),
            ),
          ],
        );
      }
    }
    return Column(
      children: [
        Expanded(
            child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                    constraints: BoxConstraints(maxWidth: 150),
                    child: Image.memory(
                      _imgnoloan,
                      fit: BoxFit.fitWidth,
                    )),
                SizedBox(
                  height: 20,
                ),
                Text(
                  _message,
                  style: TextStyle(
                      height: 1.3,
                      fontSize: 15,
                      fontFamily: 'Helvetica',
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        )),
        Container(
          padding: const EdgeInsets.all(20),
          child: _SingleButton(
            label: 'Ajukan Pinjaman',
            onTap: () async {
              _logging(_mapdata);
              if (_mapdata['loanDetails'] != null) {
                final _loanDetails = _mapdata['loanDetails'];
                if (_loanDetails['amount'] is String &&
                    _loanDetails['tempo'] is String) {
                  final _amount = _moneyFormat(_loanDetails['amount']);
                  final _respon = await Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (context, animation1, animation2) =>
                              _PengajuanPinjaman(
                                _amount,
                                _loanDetails['tempo'],
                              ),
                          transitionDuration: Duration(seconds: 0)));
                  if (_respon != null && _respon == true) {
                    loadData();
                  }
                  return;
                }
              }
              Fluttertoast.showToast(
                  msg: "Terdapat gangguan di sistem",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.TOP,
                  backgroundColor: Colors.orangeAccent,
                  textColor: Colors.white,
                  fontSize: 16.0);
            },
          ),
        )
      ],
    );
  }
}
