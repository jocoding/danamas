part of danamas;

StreamController<int> _alertsc;
StreamController<int> _provincesc;
StreamController<int> _citysc;
StreamController<int> _subdistrictsc;
StreamController<int> _villagesc;

_initStream() {
  _alertsc = StreamController.broadcast();
  _citysc = StreamController.broadcast();
  _subdistrictsc = StreamController.broadcast();
  _villagesc = StreamController.broadcast();
  _provincesc = StreamController.broadcast();
}

_disposeStream() {
  _alertsc.close();
  _citysc.close();
  _subdistrictsc.close();
  _villagesc.close();
  _provincesc.close();
}
