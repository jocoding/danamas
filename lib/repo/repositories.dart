part of danamas;

Future<void> _getSignature() async {
  final _respon = await _ApiClient()
      .apiPost('user/signature', jsonEncode({'id_reseller': _idReseller}));
  try {
    if (_respon != null) {
      final _json = _ResponModel.fromJson(_respon);
      if (_json.code == 200 && _json.status == true) {
        _signature = _json.data;
      }
    }
  } catch (e) {
    print('_getSignature: $e $_respon');
  }
}

Future<bool> _getpresetRepo() async {
  _logging('_getpresetRepo');
  final _respon = await _ApiClient().apiGet('user/preset');
  if (_respon != null) {
    final _json = _ResponModel.fromJson(_respon);
    if (_json.code == 200 && _json.status == true) {
      final _data = _json.data as List<dynamic>;
      final _dataLength = _data.length;
      Map<String, List<_PresetModel>> _temp = {};
      for (int _i = 0; _i < _dataLength; _i++) {
        final _curdat = _data[_i];
        if (_curdat['name'] is String && _curdat['name'] == 'loanDetails') {
          // loanDetails
          _mapdata[_curdat['name']] = _curdat['key'];
        } else if (_curdat['name'] is String && _curdat['key'] is List) {
          final _listdata = _curdat['key'] as List<dynamic>;
          _logging(_curdat['name']);
          List<_PresetModel> _listtemp = [];
          for (int _j = 0; _j < _listdata.length; _j++) {
            final _curdat2 = _listdata[_j];
            _listtemp.add(_PresetModel.fromJson(_curdat2));
          }
          _temp[_curdat['name']] = _listtemp;
        }
      }
      final _localPresetjson =
          jsonDecode(_jsonlocalPresetlist) as List<dynamic>;
      // _logging(_localPresetjson);
      final _localdataLength = _localPresetjson.length;

      for (int _i = 0; _i < _localdataLength; _i++) {
        final _curdat = _localPresetjson[_i];
        _logging(_curdat);
        if (_curdat['name'] is String && _curdat['key'] is List) {
          final _listdata = _curdat['key'] as List<dynamic>;
          // _logging(_curdat['name']);
          List<_PresetModel> _listtemp = [];
          for (int _j = 0; _j < _listdata.length; _j++) {
            final _curdat2 = _listdata[_j];
            _listtemp.add(_PresetModel.fromJson(_curdat2));
          }
          _temp[_curdat['name']] = _listtemp;
        }
      }
      List<_PresetModel> _listtemptgl = [];
      for (int _i = 1; _i < 32; _i++) {
        String _zeroadd = '';
        if (_i < 10) _zeroadd = '0';
        String _tgl = '$_zeroadd$_i';
        _listtemptgl.add(_PresetModel(code: _tgl, description: _tgl));
      }
      _temp['tanggal'] = _listtemptgl;
      List<_PresetModel> _listtempthn = [];
      DateTime _hariini = new DateTime.now();
      final _startyear = _hariini.year - 60;
      final _endyear = _hariini.year - 16;
      for (int _i = _startyear; _i < _endyear; _i++) {
        _listtempthn.add(_PresetModel(code: '$_i', description: '$_i'));
      }
      _temp['tahun'] = _listtempthn;
      _presetList = _temp;
      return true;
    } else if (_json.message.length > 0) {
      _logging(_json.message);
    }
  }
  return false;
}

Future<void> _getAddress(
    {@required String type, String parenttype, String parentname}) async {
  String _path = 'user/address?type=$type';
  if (parenttype != null) {
    _path = '$_path&parent_type=$parenttype&parent_name=$parentname';
  }
  final _respon = await _ApiClient().apiGet(_path);
  if (_respon != null) {
    final _json = _ResponModel.fromJson(_respon);
    if (_json.code == 200 && _json.status == true) {
      final _data = _json.data as List<dynamic>;
      final _dataLength = _data.length;
      List<_PresetModel> _listtemp = [];
      for (int _i = 0; _i < _dataLength; _i++) {
        final _curdat = _data[_i];
        if (_curdat[type] is String) {
          String _code = _curdat[type];
          if (_curdat['postal_code'] is String) {
            _code = '${_curdat['postal_code']}-${_curdat[type]}';
          }
          _listtemp.add(_PresetModel.fromJson(
              {"code": _code, "description": _curdat[type]}));
        }
      }
      _presetList[type] = _listtemp;
      if (type == 'province') _provincesc.sink.add(_unixtime());
    }
  }
}

Future<_ResponModel> _userRegister() async {
  _ResponModel _out =
      _ResponModel(status: false, message: 'Gangguan di sistem');
  final _json = {
    "name": _name,
    "email": _email,
    "idCard": _idCard,
    "gender": _gender,
    "birthPlace": _birthPlace,
    "birthDate": _birthDate,
    "mobile": _mobile,
    "businessName": _businessName,
    "businessDescription": _businessDescription,
    "businessStartYear": _businessStartYear,
    "address": _address,
    "village": _village,
    "subdistrict": _subdistrict,
    "city": _city,
    "province": _province,
    "postalCode": _postalCode,
    "businessAddress": _businessAddress,
    "businessVillage": _businessVillage,
    "businessSubdistrict": _businessSubdistrict,
    "businessCity": _businessCity,
    "businessProvince": _businessProvince,
    "businessPostalCode": _businessPostalCode,
    "createBy": _createBy,
    "profileBinaryData64String": _profileBinaryData64String,
    "profileName": _profileName,
    "profileContentType": _profileContentType,
    "binaryData64StringIdcard": _binaryData64StringIdcard,
    "profileNameIdcard": _profileNameIdcard,
    "profileContentTypeIdcard": _profileContentTypeIdcard,
    "lat": _lat,
    "lng": _lng,
    "maritalStatus": _maritalStatus,
    "education": _education,
    "religion": _religion,
    "internetBased": _internetBased,
    "workExperience": _workExperience,
    "propertyStatus": _propertyStatus,
    "businessUnit": _businessUnit,
    "job": _job,
    "salary": _salary,
    "motherMaidenName": _motherMaidenName,
    "employmentType": _employmentType,
    "emergencyName": _emergencyName,
    "emergencyRelationship": _emergencyRelationship,
    "emergencyContactNo": _emergencyContactNo,
    "emergencyAddress": _emergencyAddress
  };
  final _stringjson = jsonEncode(_json);
  _logging(_stringjson);
  final _respon = await _ApiClient().apiPost('user/register', _stringjson);

  if (_respon != null) {
    _out = _ResponModel.fromJson(_respon);
  }
  return _out;
}

Future<_ResponModel> _cekStatus(String idReseller) async {
  String _message;
  int _code;
  bool _status;
  Map<String, dynamic> _data;
  _code = 404;
  _message = 'User not registered!';
  final _respon = await _ApiClient().apiGet('users/status');
  if (_respon != null) {
    final _json = _ResponModel.fromJson(_respon);
    if (_json.code == 200 &&
        _json.status == true &&
        _json.message.trim() != 'User belum terdaftar!') {
      _code = _json.code;
      _status = _json.status;
      _data = _json.data;
      _message = _json.message;
    }
  }
  return _ResponModel(
      code: _code, status: _status, message: _message, data: _data);
}

Future<_ResponModel> _getHistory() async {
  _ResponModel _out;
  final _respon = await _ApiClient().apiGet('user/loan/history');
  if (_respon != null) {
    _out = _ResponModel.fromJson(_respon);
  }
  return _out;
}

Future<_ResponModel> _getActiveloan() async {
  _ResponModel _out;
  final _respon = await _ApiClient().apiGet('user/loan');
  if (_respon != null) {
    _out = _ResponModel.fromJson(_respon);
  }
  return _out;
}

Future<_ResponModel> _applyLoan() async {
  _ResponModel _out;
  final _respon = await _ApiClient().apiPost('user/apply', '{}');
  if (_respon != null) {
    _out = _ResponModel.fromJson(_respon);
  }
  return _out;
}
