part of danamas;

class _ApiClient {
  Dio dio = new Dio();

  // final http.Client _httpClient = http.Client();
  final Map<String, String> headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };

  Future<dynamic> apiGet(String path) async {
    Response _response;
    Map<String, String> _tempheaders = headers;
    _tempheaders.addAll({'clientid': _mapdata['clientid']});
    _tempheaders.addAll({'clientsecret': _mapdata['clientsecret']});
    if (_signature != null) _tempheaders.addAll({'signature': _signature});
    // http.Response _response;
    try {
      _logging('$path @ ${_unixtime()} $_tempheaders');
      // _response = await this
      //     ._httpClient
      //     .get(Uri.parse('$_apiurl/$path'), headers: _tempheaders);
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        _logging(options.headers);
        return options;
      }));
      _response = await dio.get('$_apiurl/$path',
          options: Options(
            headers: _tempheaders,
            validateStatus: (status) {
              return true;
            },
          ));
      // if (_response.statusCode is num) {
      //   _logging(_response.statusCode);
      //   _logging(_response.data);
      //   _logging(_response.headers);
      // }
    } catch (e) {
      _logging(e);
    }
    // if (_response != null && _response.body is String) return _response.body;
    if (_response != null) return _response.data;
    return null;
  }

  Future<dynamic> apiPost(String path, String body) async {
    Response _response;
    Map<String, String> _tempheaders = headers;
    _tempheaders.addAll({'clientid': _mapdata['clientid']});
    _tempheaders.addAll({'clientsecret': _mapdata['clientsecret']});
    if (_signature != null) _tempheaders.addAll({'signature': _signature});
    // http.Response _response;
    try {
      _logging('$path @ ${_unixtime()} $_tempheaders');
      // _response = await this
      //     ._httpClient
      //     .post(Uri.parse('$_apiurl/$path'), headers: _tempheaders, body: body);

      _response = await dio.post('$_apiurl/$path',
          data: body,
          options: Options(
            headers: _tempheaders,
            validateStatus: (status) {
              return true;
            },
          ));
      // if (_response.statusCode is num) {
      //   _logging(_response.statusCode);
      //   _logging(_response.data);
      //   _logging(_response.headers);
      //   final _per = 2000;
      //   final _jum = (body.length / _per).ceil();
      //   int awal = 0;
      //   for (int i = 0; i < _jum; i++) {
      //     final akhir = awal + _per;
      //     String str;
      //     if (akhir > body.length)
      //       str = body.substring(awal);
      //     else
      //       str = body.substring(awal, akhir);
      //     awal = akhir;
      //     _logging('|$str|');
      //   }
      // }
    } catch (e) {
      _logging(e);
    }
    // if (_response != null && _response.body is String) return _response.body;
    if (_response != null) return _response.data;
    return null;
  }
}
