part of danamas;

int _unixtime() {
  return (new DateTime.now().millisecondsSinceEpoch / 1000).floor();
}

void _logging(Object log) {
  print('Danamas: $log');
}

String _moneyFormat(dynamic angka) {
  double _angka = double.parse('$angka');
  final _numfor = new NumberFormat("#,##0.00", "en_US");
  return '${_numfor.format(_angka)}'
      .replaceAllMapped(new RegExp(r'.([0-9]{2})$'), (match) => ';${match[1]}')
      .replaceAll(new RegExp(r';00$'), '')
      .replaceAll(new RegExp(r','), '.')
      .replaceAll(new RegExp(r';'), ',');
}

String _dateformat1(DateTime tanggal) {
  String _out = DateFormat('dd MMMM yyyy').format(tanggal);
  final Map<String, String> _maptgl = {
    'January': 'Januari',
    'February': 'Februari',
    'March': 'Maret',
    'April': 'April',
    'May': 'Mei',
    'June': 'Juni',
    'July': 'Juli',
    'August': 'Agustus',
    'September': 'September',
    'October': 'Oktober',
    'November': 'November',
    'December': 'Desember'
  };
  return _out.replaceAllMapped(new RegExp(r' ([A-Za-z]+) '),
      (match) => ' ${_maptgl[match[1]] ?? match[1]} ');
}
