part of danamas;

class _RegistrasiDuaFoto extends StatefulWidget {
  @override
  _RegistrasiDuaFotoState createState() => _RegistrasiDuaFotoState();
}

class _RegistrasiDuaFotoState extends State<_RegistrasiDuaFoto> {
  StreamController<int> _imgsc = StreamController.broadcast();

  @override
  void dispose() {
    _imgsc.close();
    super.dispose();
  }

  void _prosesImage(PickedFile _respon) async {
    _logging(_respon.path);
    _profileContentType = lookupMimeType(_respon.path);
    final _uuid = _puuid.Uuid();
    String _extensi = _respon.path.split(".").last;
    if (_extensi == "") {
      switch (_extensi) {
        case 'image/jpeg':
          _extensi = 'jpg';
          break;
        case 'image/gif':
          _extensi = 'gif';
          break;
        case 'image/png':
          _extensi = 'gif';
          break;
        default:
          _extensi = 'jpg';
      }
    }
    _profileName = '${_uuid.v1()}.$_extensi';
    _logging('$_profileName $_profileContentType');
    final bytes = await File(_respon.path).readAsBytes();
    _profileBinaryData64String = base64Encode(bytes);
    _imgsc.sink.add(_unixtime());
  }

  Widget _listnote(String msg) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 2, right: 10),
          width: 4,
          height: 4,
          decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.all(Radius.circular(2))),
        ),
        Expanded(
            child: Text(
          msg,
          style: TextStyle(fontFamily: 'Helvetica', fontSize: 11),
        )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _AppBar(
          context: context,
          title: 'Upload Foto Diri',
          subtitle: 'Selanjutnya: Upload Kartu Identitas',
          imgstep: _imgstepdua),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    StreamBuilder<int>(
                        stream: _imgsc.stream,
                        builder: (context, snapshot) {
                          if (_profileBinaryData64String != null &&
                              _profileBinaryData64String != '') {
                            final _imgFoto =
                                base64Decode(_profileBinaryData64String);
                            return Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: _boldGrey)),
                                padding: const EdgeInsets.all(10),
                                constraints: BoxConstraints(
                                    maxWidth: 300, maxHeight: 200),
                                child: SingleChildScrollView(
                                  child: Image.memory(_imgFoto,
                                      fit: BoxFit.contain),
                                ));
                          }
                          return Container(
                              constraints: BoxConstraints(maxWidth: 140),
                              child: Image.memory(_imgprofilepict,
                                  fit: BoxFit.contain));
                        }),
                    SizedBox(height: 20),
                    Text('Upload Foto Diri',
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Helvetica',
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 20),
                      padding: const EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          border: Border.all(color: _boldGrey),
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          _listnote(
                              'Pastikan foto terlihat jelas dan tidak blur'),
                          _listnote(
                              'Pastikan foto tidak terpotong atau ada bagian yang tertutupi'),
                          _listnote(
                              'Pastikan foto asli bukan hasil scan atau fotocopy'),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        children: [
                          Expanded(
                              child: _OutLineBtn(
                            label: 'Upload',
                            margin: const EdgeInsets.only(right: 5),
                            onTap: () async {
                              if (await Permission.storage
                                      .request()
                                      .isGranted ==
                                  true) {
                                final _respon = await ImagePicker().getImage(
                                    source: ImageSource.gallery,
                                    maxHeight: 500,
                                    maxWidth: 500);
                                _prosesImage(_respon);

                                // await _uploadImage(_respon.path);
                              }
                            },
                          )),
                          Expanded(
                              child: _OutLineBtn(
                            label: 'Gunakan Kamera',
                            margin: const EdgeInsets.only(left: 5),
                            onTap: () async {
                              if (await Permission.camera.request().isGranted ==
                                  true) {
                                final _respon = await ImagePicker().getImage(
                                    source: ImageSource.camera,
                                    maxHeight: 500,
                                    maxWidth: 500);

                                _prosesImage(_respon);
                              }
                            },
                          )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Row(
              children: [
                _BackButton(
                  child: Icon(
                    Icons.arrow_back,
                    color: _boldGrey,
                    size: 20,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: _WithArrowButton(
                    label: 'Selanjutnya',
                    onTap: () async {
                      final _respon = await Navigator.push(
                          context,
                          PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  _RegistrasiTiga(),
                              transitionDuration:
                                  Duration(seconds: 0))) as _ResponModel;
                      if (_respon != null) Navigator.of(context).pop(_respon);
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
