part of danamas;

class _PinjamanPage extends StatefulWidget {
  final Uint8List image;
  final String title;
  final String subtitle;
  final String buttonlabel;
  final Function onTap;
  _PinjamanPage(
      {@required this.image,
      @required this.title,
      this.subtitle,
      this.buttonlabel,
      this.onTap});
  @override
  _PinjamanState createState() => _PinjamanState();
}

class _PinjamanState extends State<_PinjamanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: _primaryRed,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            InkResponse(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back,
                size: 20,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Pinjaman',
              style: TextStyle(fontFamily: 'VarelaRound', fontSize: 18),
            ),
          ],
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                          constraints: BoxConstraints(maxWidth: 150),
                          child: Image.memory(
                            widget.image,
                            fit: BoxFit.fitWidth,
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        widget.title,
                        style: TextStyle(
                            height: 1.3,
                            fontSize: 15,
                            fontFamily: 'Helvetica',
                            fontWeight: FontWeight.bold),
                      ),
                      widget.subtitle == null
                          ? Container()
                          : Container(
                              constraints: BoxConstraints(maxWidth: 260),
                              child: Text(
                                widget.subtitle,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  height: 1.3,
                                  fontSize: 15,
                                  fontFamily: 'Helvetica',
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
            widget.buttonlabel == null
                ? Container()
                : Container(
                    padding: const EdgeInsets.all(20),
                    child: _SingleButton(
                      label: widget.buttonlabel,
                      onTap: widget.onTap,
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
