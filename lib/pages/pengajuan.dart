part of danamas;

class _ListSyaratKetentuan extends StatelessWidget {
  final String teks;
  _ListSyaratKetentuan(this.teks);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            width: 6,
            height: 6,
            margin: const EdgeInsets.only(right: 10, top: 7),
            decoration: BoxDecoration(
                color: _midGrey,
                borderRadius: BorderRadius.all(Radius.circular(3)))),
        Expanded(
          child: Text(
            teks,
            style: TextStyle(
                fontFamily: 'VarelaRound', fontSize: 14, color: _midGrey),
          ),
        ),
      ],
    );
  }
}

class _PengajuanPinjaman extends StatelessWidget {
  final String amount;
  final String tempo;
  _PengajuanPinjaman(this.amount, this.tempo);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: _primaryRed,
        elevation: 0.0,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            InkResponse(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back,
                size: 20,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Pinjaman',
              style: TextStyle(fontFamily: 'VarelaRound', fontSize: 18),
            ),
          ],
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(width: double.infinity, height: double.infinity),
                  Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      child: Container(height: 100, color: _primaryRed)),
                  Container(
                    margin: const EdgeInsets.only(left: 20, right: 20, top: 30),
                    width: double.infinity,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: _lightGrey,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: SingleChildScrollView(
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Text(
                              'Jumlah Pinjaman',
                              style: TextStyle(
                                  fontFamily: 'VarelaRound',
                                  fontSize: 11,
                                  color: _boldGrey),
                            ),
                            SizedBox(height: 10),
                            Text(
                              amount,
                              style: TextStyle(
                                  fontFamily: 'VarelaRound',
                                  fontSize: 14,
                                  color: Colors.black),
                            ),
                            SizedBox(height: 30),
                            Text(
                              'Tempo Pinjaman',
                              style: TextStyle(
                                  fontFamily: 'VarelaRound',
                                  fontSize: 11,
                                  color: _boldGrey),
                            ),
                            SizedBox(height: 10),
                            Text(
                              '$tempo Hari',
                              style: TextStyle(
                                  fontFamily: 'VarelaRound',
                                  fontSize: 14,
                                  color: Colors.black),
                            ),
                            SizedBox(height: 40),
                            _SingleButton(
                              label: 'Ajukan Pinjaman',
                              onTap: () async {
                                final _respon = await _applyLoan();
                                if (_respon.code == 200 &&
                                    _respon.status == true)
                                  Navigator.of(context).pop(true);
                                else if (_respon.message is String) {
                                  _alertWarning(
                                      context, _respon.message, 'Kembali');
                                }
                              },
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(20),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Syarat dan Ketentuan:',
                      style: TextStyle(
                          fontFamily: 'VarelaRound',
                          fontSize: 14,
                          color: Colors.black),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    _ListSyaratKetentuan(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
                    _ListSyaratKetentuan(
                        'Ultricies dignissim quam vitae mollis sociis morbi facilisi sollicitudin.'),
                    _ListSyaratKetentuan(
                        'Sit duis arcu faucibus facilisis mattis platea leo.'),
                    _ListSyaratKetentuan(
                        'Scelerisque at tincidunt donec faucibus mi tristique in.'),
                    SizedBox(
                      height: 20,
                    )
                  ]),
            )
          ],
        ),
      ),
    );
  }
}
