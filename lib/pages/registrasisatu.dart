part of danamas;

class _RegistrasiSatu extends StatefulWidget {
  @override
  _RSState createState() => _RSState();
}

class _RSState extends State<_RegistrasiSatu> {
  TextEditingController _idresellerctr = TextEditingController();
  TextEditingController _emailctr = TextEditingController();
  TextEditingController _passwordctr = TextEditingController();

  @override
  void initState() {
    if (_idReseller != null) _idresellerctr.text = _idReseller;
    if (_email != null) _emailctr.text = _email;
    if (_password != null) _passwordctr.text = _password;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('build _RegistrasiSatu');
    return Scaffold(
      appBar: _AppBar(
          context: context,
          title: 'Registrasi Pinjaman',
          subtitle: 'Selanjutnya: Form Data Diri',
          imgstep: _imgstepsatu),
      body: InkResponse(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Lengkapi data berikut untuk pengajuan pinjaman:',
                        style: TextStyle(fontSize: 13, fontFamily: 'Helvetica'),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _TextField(
                        labelText: 'ID Reseller',
                        controller: _idresellerctr,
                        readOnly: true,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _TextField(
                        labelText: 'Email',
                        controller: _emailctr,
                        keyboardType: TextInputType.emailAddress,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _TextField(
                        labelText: 'Password',
                        controller: _passwordctr,
                        obscureText: true,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
              _WithArrowButton(
                label: 'Selanjutnya',
                onTap: () async {
                  RegExp _regExp = new RegExp(
                    r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$",
                    caseSensitive: true,
                    multiLine: false,
                  );
                  if (_idresellerctr.text.trim() == '' ||
                      _passwordctr.text.trim() == '' ||
                      _emailctr.text.trim() == '') {
                    _alertWarning(context, 'Mohon lengkapi isian', 'Ulangi');
                    return;
                  }
                  if (_regExp.hasMatch(_emailctr.text.trim()) == false) {
                    _alertWarning(context, 'Email tidak valid', 'Ulangi');
                    return;
                  }
                  _email = _emailctr.text.trim();
                  _password = _passwordctr.text.trim();
                  final _respon = await Navigator.push(
                          context,
                          PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  _RegistrasiDua(),
                              transitionDuration: Duration(seconds: 0)))
                      as _ResponModel;
                  if (_respon != null && _respon.code != 201) {
                    Navigator.of(context).pop(_respon);
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
