part of danamas;

class DanamasPage extends StatefulWidget {
  final String idReseller;
  final String apiurl;
  final String clientid;
  final String clientsecret;
  DanamasPage(
      {@required this.idReseller,
      @required this.apiurl,
      @required this.clientid,
      @required this.clientsecret});
  @override
  _DanamasState createState() => _DanamasState();
}

class _DanamasState extends State<DanamasPage> {
  StreamController<int> _reload = StreamController();
  LocationData _locationData;
  bool _isinitloading = true;

  void loading() async {
    await Future.delayed(Duration(milliseconds: 50));
    _showLoadingDialog(context);
  }

  void initSync() async {
    _logging('Start initSync');
    loading();
    await _getSignature();
    await Future.delayed(Duration(milliseconds: 50));
    _closeLoading(context);
    if (_signature == null || _signature == '') {
      await _alertWarning(
          context, 'Terdapat gangguan di sistem kami', 'Kembali');
      Navigator.of(context).pop();
      return;
    }
    if (await _getpresetRepo() == false) {
      await _alertWarning(context, 'Akses tidak diperkenankan', 'Kembali');
      Navigator.of(context).pop();
      return;
    }
    _showLoadingDialog(context);
    _getAddress(type: 'province');
    _closeLoading(context);
    if (await Permission.location.status.isGranted) {
      await locationAllowed();
    }
    _isinitloading = false;
    _reload.sink.add(_unixtime());
  }

  Future<void> locationAllowed() async {
    _logging('Permission.location.request()');
    if (await Permission.location.request().isGranted) {
      _showLoadingDialog(context);
      Location location = new Location();
      bool _serviceEnabled;
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          Navigator.of(context).pop();
          return;
        }
      }
      _logging('Granted');
      _locationData = await location.getLocation();
      _logging(_locationData.accuracy);
      _lat = '${_locationData.latitude}';
      _lng = '${_locationData.longitude}';
      _closeLoading(context);
      _ResponModel _respon;
      _respon = await _cekStatus(widget.idReseller);
      if (_respon.code == 200 && _respon.status == true) {
        final String _isVerified = _respon.data["isVerified"] ?? "0";
        final String _isApproved = _respon.data["isApproved"] ?? "0";
        if ((_isVerified == "0" || _isVerified == "1") && _isApproved == "0") {
          _respon = await Navigator.push(
              context,
              PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) =>
                      _PinjamanPage(
                        image: _imgsuccessregister,
                        title: 'Registrasi Berhasil.',
                        subtitle: _respon.message,
                      ),
                  transitionDuration: Duration(seconds: 0))) as _ResponModel;
          if (_respon == null) Navigator.of(context).pop();
        } else if (_isVerified == "-1" || _isApproved == "-1") {
          _respon = await Navigator.push(
              context,
              PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) =>
                      _PinjamanPage(
                        image: _imgfailedregis,
                        title: _respon.message,
                        buttonlabel: 'Registrasi Ulang',
                        onTap: () async {
                          final _respon = await Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                      pageBuilder:
                                          (context, animation1, animation2) =>
                                              _RegistrasiDua(),
                                      transitionDuration: Duration(seconds: 0)))
                              as _ResponModel;
                          Navigator.of(context).pop(_respon);
                        },
                      ),
                  transitionDuration: Duration(seconds: 0))) as _ResponModel;
          if (_respon != null &&
              _respon.code == 200 &&
              _respon.status == true) {
            _respon = await Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) =>
                        _PinjamanPage(
                          image: _imgsuccessregister,
                          title: _respon.message,
                        ),
                    transitionDuration: Duration(seconds: 0))) as _ResponModel;
          }
          Navigator.of(context).pop();
        } else if (_isVerified == "1" && _isApproved == "1") {
          _respon = await Navigator.push(
              context,
              PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) =>
                      _HistoryPage(),
                  transitionDuration: Duration(seconds: 0))) as _ResponModel;
          if (_respon == null) Navigator.of(context).pop();
        }
      } else {
        _respon = await Navigator.push(
            context,
            PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => _PinjamanPage(
                      image: _imgneedregister,
                      title: 'Anda belum melakukan registrasi.',
                      subtitle:
                          'Tekan tombol Registrasi untuk melanjutkan tahap pinjaman Anda.',
                      buttonlabel: 'Registrasi',
                      onTap: () async {
                        final _respon = await Navigator.push(
                                context,
                                PageRouteBuilder(
                                    pageBuilder:
                                        (context, animation1, animation2) =>
                                            _RegistrasiDua(),
                                    transitionDuration: Duration(seconds: 0)))
                            as _ResponModel;
                        Navigator.of(context).pop(_respon);
                      },
                    ),
                transitionDuration: Duration(seconds: 0))) as _ResponModel;
        if (_respon != null && _respon.code == 200 && _respon.status == true) {
          _respon = await Navigator.push(
              context,
              PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) =>
                      _PinjamanPage(
                        image: _imgsuccessregister,
                        title: _respon.message,
                      ),
                  transitionDuration: Duration(seconds: 0))) as _ResponModel;
        }
        Navigator.of(context).pop();
      }

      return;
    }
    _logging((await Permission.location.status).toString());
    _closeLoading(context);
    Navigator.of(context).pop(false);
  }

  @override
  void initState() {
    _mapdata['clientid'] = widget.clientid;
    _mapdata['clientsecret'] = widget.clientsecret;
    _createBy = 'PulsaPro';
    _idReseller = widget.idReseller;
    _apiurl = widget.apiurl;
    _initStream();
    _initAsset();
    initSync();
    super.initState();
  }

  @override
  void dispose() {
    _disposeStream();
    _reload.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _signature = null;
        return true;
      },
      child: Scaffold(
        body: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.white,
            padding: const EdgeInsets.all(30),
            child: Column(
              children: [
                Expanded(
                    child: Center(
                        child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                          constraints: BoxConstraints(maxWidth: 150),
                          child: Image.memory(
                            _imglocation,
                            fit: BoxFit.fitWidth,
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Izinkan Penggunaan GPS / Lokasi Anda',
                        style: TextStyle(
                            height: 1.3,
                            fontSize: 15,
                            fontFamily: 'Helvetica',
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ))),
                StreamBuilder<int>(
                    stream: _reload.stream,
                    builder: (context, snapshot) {
                      if (_isinitloading == true) return Container();
                      return Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            _SingleButton(
                              label: 'IJINKAN',
                              onTap: locationAllowed,
                            ),
                            InkResponse(
                              child: Container(
                                padding: const EdgeInsets.only(
                                    top: 20, left: 20, right: 20),
                                child: Text(
                                  'Lewati',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'VarelaRound',
                                      color: _primaryRed),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        ),
                      );
                    })
              ],
            )),
      ),
    );
  }
}
