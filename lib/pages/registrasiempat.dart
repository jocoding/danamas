part of danamas;

class _RegistrasiEmpat extends StatefulWidget {
  @override
  _RegistrasiEmpatState createState() => _RegistrasiEmpatState();
}

class _RegistrasiEmpatState extends State<_RegistrasiEmpat> {
  TextEditingController _businessNamectr = TextEditingController();
  TextEditingController _businessDescriptionctr = TextEditingController();
  TextEditingController _businessStartYearctr = TextEditingController();
  TextEditingController _businessAddressctr = TextEditingController();
  TextEditingController _provincectr = TextEditingController();
  TextEditingController _cityctr = TextEditingController();
  TextEditingController _subdistrictctr = TextEditingController();
  TextEditingController _villagectr = TextEditingController();
  TextEditingController _postalCodectr = TextEditingController();

  List<_PresetModel> _tahunlist = [];
  List<_PresetModel> _belumadapilihan = [
    _PresetModel(code: '', description: 'pilihan belum tersedia')
  ];

  Future<void> resyncAddress() async {
    if (_businessProvince != null) {
      await Future.delayed(Duration(milliseconds: 50));
      _showLoadingDialog(context);
      await _getAddress(
          type: 'kota_kab',
          parenttype: 'province',
          parentname: _businessProvince);
      _citysc.sink.add(_unixtime());
      if (_businessCity != null) {
        await _getAddress(
            type: 'kecamatan',
            parenttype: 'kota_kab',
            parentname: _businessCity);
        _subdistrictsc.sink.add(_unixtime());
        if (_businessSubdistrict != null) {
          await _getAddress(
              type: 'kelurahan',
              parenttype: 'kecamatan',
              parentname: _businessSubdistrict);
          _villagesc.sink.add(_unixtime());
        }
      }
      _closeLoading(context);
    }
  }

  void initSync() async {
    final _hariini = new DateTime.now();
    final _startyear = _hariini.year - 60;
    final _endyear = _hariini.year;
    for (int _i = _endyear; _i > _startyear; _i--) {
      _tahunlist.add(_PresetModel(code: '$_i', description: '$_i'));
    }
    if (_presetList['province'] == null ||
        _presetList['province'].length == 0) {
      await _getAddress(type: 'province');
    }
    await resyncAddress();

    if (_businessName != null) _businessNamectr.text = _businessName;
    if (_businessDescription != null)
      _businessDescriptionctr.text = _businessDescription;
    if (_businessAddress != null) _businessAddressctr.text = _businessAddress;
    if (_businessPostalCode != null) _postalCodectr.text = _businessPostalCode;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    initSync();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _AppBar(
          context: context,
          title: 'Form Data Perusahaan',
          subtitle: 'Selanjutnya: Selesai',
          imgstep: _imgstepempat),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'DATA PERUSAHAAN',
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: 'Helvetica',
                        color: _primaryRed),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _TextField(
                    labelText: 'Nama Perusahaan',
                    controller: _businessNamectr,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _TextField(
                    labelText: 'Deskripsi Perusahaan',
                    controller: _businessDescriptionctr,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Tahun Pembuatan',
                    style: TextStyle(fontSize: 11, color: _boldGrey),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: _SelectField(
                            labelText: 'Tahun',
                            controller: _businessStartYearctr,
                            listdata: _tahunlist,
                            value: _businessStartYear,
                            onChanged: (String _val) {
                              _businessStartYear = _val;
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            }),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _TextField(
                    labelText: 'Alamat Perusahaan',
                    controller: _businessAddressctr,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  StreamBuilder<int>(
                      stream: _provincesc.stream,
                      builder: (context, snapshot) {
                        return _SelectField(
                            labelText: 'Provinsi',
                            controller: _provincectr,
                            listdata:
                                _presetList['province'] ?? _belumadapilihan,
                            value: _businessProvince,
                            onChanged: (String _val) async {
                              if (_businessProvince != _val) {
                                _logging('_SelectField Provinsi');
                                _businessProvince = _val;
                                _presetList['kota_kab'] = _belumadapilihan;
                                _presetList['kecamatan'] = _belumadapilihan;
                                _presetList['kelurahan'] = _belumadapilihan;
                                _cityctr.text = '';
                                _subdistrictctr.text = '';
                                _villagectr.text = '';
                                _showLoadingDialog(context);
                                await _getAddress(
                                    type: 'kota_kab',
                                    parenttype: 'province',
                                    parentname: _val);
                                _citysc.sink.add(_unixtime());
                                _subdistrictsc.sink.add(_unixtime());
                                _villagesc.sink.add(_unixtime());
                                _closeLoading(context);
                              }

                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            });
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  StreamBuilder<int>(
                      stream: _citysc.stream,
                      builder: (context, snapshot) {
                        return _SelectField(
                            labelText: 'Kota/Kabupaten',
                            controller: _cityctr,
                            listdata:
                                _presetList['kota_kab'] ?? _belumadapilihan,
                            value: _businessCity,
                            onChanged: (String _val) async {
                              if (_businessCity != _val) {
                                _businessCity = _val;
                                _presetList['kecamatan'] = _belumadapilihan;
                                _presetList['kelurahan'] = _belumadapilihan;
                                _subdistrictctr.text = '';
                                _villagectr.text = '';
                                _showLoadingDialog(context);
                                await _getAddress(
                                    type: 'kecamatan',
                                    parenttype: 'kota_kab',
                                    parentname: _val);
                                _subdistrictsc.sink.add(_unixtime());
                                _villagesc.sink.add(_unixtime());
                                _closeLoading(context);
                              }
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            });
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  StreamBuilder<int>(
                      stream: _subdistrictsc.stream,
                      builder: (context, snapshot) {
                        return _SelectField(
                            labelText: 'Kecamatan',
                            controller: _subdistrictctr,
                            listdata:
                                _presetList['kecamatan'] ?? _belumadapilihan,
                            value: _businessSubdistrict,
                            onChanged: (String _val) async {
                              if (_businessSubdistrict != _val) {
                                _businessSubdistrict = _val;
                                _presetList['kelurahan'] = _belumadapilihan;
                                _villagectr.text = '';
                                _showLoadingDialog(context);
                                await _getAddress(
                                    type: 'kelurahan',
                                    parenttype: 'kecamatan',
                                    parentname: _val);
                                _villagesc.sink.add(_unixtime());
                                _closeLoading(context);
                              }
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            });
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  StreamBuilder<int>(
                      stream: _villagesc.stream,
                      builder: (context, snapshot) {
                        final _curval =
                            '$_businessPostalCode-$_businessVillage';
                        return _SelectField(
                            labelText: 'Kelurahan',
                            controller: _villagectr,
                            listdata: _presetList['kelurahan'] ?? [],
                            value: _curval,
                            onChanged: (String _val) async {
                              if (_curval != _val) {
                                try {
                                  RegExp _regExp = new RegExp(
                                    r"([0-9]+)\-(.*)",
                                    caseSensitive: true,
                                    multiLine: false,
                                  );
                                  final _res = _regExp.allMatches(_val);
                                  final _matches = _res.elementAt(0);
                                  final _curcode = _matches.group(1);
                                  _val = _curcode;
                                } catch (e) {
                                  _logging(e);
                                }
                                _businessPostalCode = _val;
                                _postalCodectr.text = _val;
                                _businessVillage = _villagectr.text;
                                _logging(
                                    '$_businessPostalCode $_businessVillage');
                              }
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            });
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  _TextField(
                    labelText: 'Kode Post',
                    controller: _postalCodectr,
                    readOnly: true,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            )),
            Row(
              children: [
                _BackButton(
                  child: Icon(
                    Icons.arrow_back,
                    color: _boldGrey,
                    size: 20,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: _WithArrowButton(
                    icon: Icons.check,
                    label: 'Registrasi Sekarang',
                    onTap: () async {
                      if (_businessNamectr.text.trim() == '' ||
                          _businessDescriptionctr.text.trim() == '' ||
                          _businessStartYear == '' ||
                          _businessAddressctr.text.trim() == '' ||
                          _businessProvince == '' ||
                          _businessCity == '' ||
                          _businessSubdistrict == '' ||
                          _businessVillage == '' ||
                          _businessPostalCode == '') {
                        _alertWarning(
                            context, 'Mohon lengkapi isian', 'Ulangi');
                        return;
                      }
                      _businessName = _businessNamectr.text.trim();
                      _businessDescription =
                          _businessDescriptionctr.text.trim();
                      _businessAddress = _businessAddressctr.text.trim();
                      _showLoadingDialog(context);
                      _ResponModel _apirespon = await _userRegister();
                      _closeLoading(context);
                      if (_apirespon.code != 200 ||
                          _apirespon.status == false) {
                        _closeLoading(context);
                        final _respon = await Navigator.push(
                            context,
                            PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                        _PinjamanPage(
                                          image: _imgalertwarning,
                                          title: _apirespon.message ??
                                              'Registrasi gagal',
                                          buttonlabel: 'Registrasi Ulang',
                                          onTap: () {
                                            Navigator.of(context).pop(true);
                                          },
                                        ),
                                transitionDuration: Duration(seconds: 0)));
                        if (_respon != null && _respon == true) {
                          _apirespon = _ResponModel(code: 201);
                        } else {
                          return;
                        }
                      }
                      Navigator.of(context).pop(_apirespon);
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
