part of danamas;

class _HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

enum _Tab { AKTIF, HISTORY }

class _HistoryPageState extends State<_HistoryPage> {
  _Tab _activetab = _Tab.AKTIF;
  StreamController<int> _reload = StreamController.broadcast();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _reload.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: _primaryRed,
        elevation: 0.0,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            InkResponse(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back,
                size: 20,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Pinjaman',
              style: TextStyle(fontFamily: 'VarelaRound', fontSize: 18),
            ),
          ],
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            StreamBuilder<int>(
                stream: _reload.stream,
                builder: (context, snapshot) {
                  final _aktifColor =
                      _activetab == _Tab.AKTIF ? Colors.white : Colors.white60;
                  final _historyColor = _activetab == _Tab.HISTORY
                      ? Colors.white
                      : Colors.white60;
                  return Row(
                    children: [
                      Expanded(
                        child: InkResponse(
                          onTap: () async {
                            _activetab = _Tab.AKTIF;
                            _reload.sink.add(_unixtime());
                          },
                          child: Container(
                              color: _primaryRed,
                              padding:
                                  const EdgeInsets.only(bottom: 15, top: 5),
                              child: Center(
                                child: Text('PINJAMAN AKTIF',
                                    style: TextStyle(
                                        fontFamily: 'VarelaRound',
                                        fontSize: 11,
                                        color: _aktifColor)),
                              )),
                        ),
                      ),
                      Expanded(
                        child: InkResponse(
                          onTap: () async {
                            _activetab = _Tab.HISTORY;
                            _reload.sink.add(_unixtime());
                          },
                          child: Container(
                              color: _primaryRed,
                              padding:
                                  const EdgeInsets.only(bottom: 15, top: 5),
                              child: Center(
                                child: Text('HISTORY PINJAMAN',
                                    style: TextStyle(
                                        fontFamily: 'VarelaRound',
                                        fontSize: 11,
                                        color: _historyColor)),
                              )),
                        ),
                      ),
                    ],
                  );
                }),
            Expanded(
                child: Container(
              width: double.infinity,
              height: double.infinity,
              child: StreamBuilder<int>(
                  stream: _reload.stream,
                  builder: (context, snapshot) {
                    // return Container();
                    if (_activetab == _Tab.AKTIF) return _PinjamanAktif();
                    return _HistoryPinjaman();
                  }),
            ))
          ],
        ),
      ),
    );
  }
}
