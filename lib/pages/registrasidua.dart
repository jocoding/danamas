part of danamas;

class _RegistrasiDua extends StatefulWidget {
  @override
  _RegistrasiDuaState createState() => _RegistrasiDuaState();
}

class _RegistrasiDuaState extends State<_RegistrasiDua> {
  TextEditingController _namectr = TextEditingController();
  TextEditingController _emailctr = TextEditingController();
  TextEditingController _idCardctr = TextEditingController();
  TextEditingController _genderctr = TextEditingController();
  TextEditingController _birthPlacectr = TextEditingController();
  TextEditingController _tgllahirctr = TextEditingController();
  TextEditingController _blnlahirctr = TextEditingController();
  TextEditingController _thnlahirctr = TextEditingController();
  TextEditingController _mobilectr = TextEditingController();
  TextEditingController _addressctr = TextEditingController();
  TextEditingController _provincectr = TextEditingController();
  TextEditingController _cityctr = TextEditingController();
  TextEditingController _subdistrictctr = TextEditingController();
  TextEditingController _villagectr = TextEditingController();
  TextEditingController _postalCodectr = TextEditingController();
  TextEditingController _maritalStatusctr = TextEditingController();
  TextEditingController _educationctr = TextEditingController();
  TextEditingController _religionctr = TextEditingController();
  TextEditingController _internetBasedctr = TextEditingController();
  TextEditingController _workExperiencectr = TextEditingController();
  TextEditingController _jobctr = TextEditingController();
  TextEditingController _salaryctr = TextEditingController();
  TextEditingController _employmentTypectr = TextEditingController();
  TextEditingController _propertyStatusctr = TextEditingController();
  TextEditingController _businessUnitctr = TextEditingController();
  TextEditingController _motherMaidenNamectr = TextEditingController();
  TextEditingController _emergencyNamectr = TextEditingController();
  TextEditingController _emergencyRelationshipctr = TextEditingController();
  TextEditingController _emergencyContactNoctr = TextEditingController();
  TextEditingController _emergencyAddressctr = TextEditingController();
  StreamController<int> _reload = StreamController.broadcast();

  String _rdstgllahir = '';
  String _rdsblnlahir = '';
  String _rdsthnlahir = '';
  RegExp _regExptgl;
  List<_PresetModel> _belumadapilihan = [
    _PresetModel(code: '', description: 'pilihan belum tersedia')
  ];

  Future<void> resyncAddress() async {
    if (_province != null) {
      await _getAddress(
          type: 'kota_kab', parenttype: 'province', parentname: _province);
      _citysc.sink.add(_unixtime());
      if (_city != null) {
        await _getAddress(
            type: 'kecamatan', parenttype: 'kota_kab', parentname: _city);
        _subdistrictsc.sink.add(_unixtime());
        if (_subdistrict != null) {
          await _getAddress(
              type: 'kelurahan',
              parenttype: 'kecamatan',
              parentname: _subdistrict);
          _villagesc.sink.add(_unixtime());
        }
      }
    }
    _closeLoading(context);
  }

  void initSync() async {
    await Future.delayed(Duration(milliseconds: 50));
    _showLoadingDialog(context);
    if (_presetList['province'] == null ||
        _presetList['province'].length == 0) {
      await _getAddress(type: 'province');
    }

    _regExptgl = new RegExp(
      r"^[0-9]{8}$",
      multiLine: false,
    );
    if (_name != null) _namectr.text = _name;
    if (_email != null) _emailctr.text = _email;
    if (_idCard != null) _idCardctr.text = _idCard;
    if (_birthPlace != null) _birthPlacectr.text = _birthPlace;
    if (_mobile != null) _mobilectr.text = _mobile;
    if (_address != null) _addressctr.text = _address;
    if (_birthDate != null && _regExptgl.hasMatch(_birthDate) == true) {
      _rdstgllahir = _birthDate.substring(0, 2);
      _rdsblnlahir = _birthDate.substring(2, 4);
      _rdsthnlahir = _birthDate.substring(4);
    }
    if (_postalCode != null) _postalCodectr.text = _postalCode;
    if (_education != null) _educationctr.text = _education;
    if (_religion != null) _religionctr.text = _religion;
    if (_internetBased != null) _internetBasedctr.text = _internetBased;
    if (_workExperience != null) _workExperiencectr.text = _workExperience;
    if (_job != null) _jobctr.text = _job;
    if (_salary != null) _salaryctr.text = _salary;
    if (_employmentType != null) _employmentTypectr.text = _employmentType;
    if (_propertyStatus != null) _propertyStatusctr.text = _propertyStatus;
    if (_businessUnit != null) _businessUnitctr.text = _businessUnit;
    if (_motherMaidenName != null)
      _motherMaidenNamectr.text = _motherMaidenName;
    if (_emergencyName != null) _emergencyNamectr.text = _emergencyName;
    if (_emergencyRelationship != null)
      _emergencyRelationshipctr.text = _emergencyRelationship;
    if (_emergencyContactNo != null)
      _emergencyContactNoctr.text = _emergencyContactNo;
    if (_emergencyAddress != null)
      _emergencyAddressctr.text = _emergencyAddress;
    _reload.sink.add(_unixtime());
    resyncAddress();
  }

  @override
  void initState() {
    initSync();
    super.initState();
  }

  @override
  void dispose() {
    _reload.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _logging('build _RegistrasiDua');
    return Scaffold(
      appBar: _AppBar(
          context: context,
          title: 'Form Data Diri',
          subtitle: 'Selanjutnya: Upload Foto Diri',
          imgstep: _imgstepsatu),
      body: InkResponse(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          padding: const EdgeInsets.all(20),
          child: StreamBuilder<int>(
              stream: _reload.stream,
              builder: (context, snapshot) {
                return Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              'DATA PRIBADI',
                              style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: 'Helvetica',
                                  color: _primaryRed),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Nama',
                              controller: _namectr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Email',
                              controller: _emailctr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Nomor KTP',
                              controller: _idCardctr,
                              keyboardType: TextInputType.number,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Jenis Kelamin',
                                controller: _genderctr,
                                listdata: _presetList['gender'] ?? [],
                                value: _gender,
                                onChanged: (String _val) {
                                  _gender = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Kota Lahir',
                              controller: _birthPlacectr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'Tanggal Lahir',
                              style: TextStyle(fontSize: 11, color: _boldGrey),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: _SelectField(
                                      labelText: 'Tanggal',
                                      controller: _tgllahirctr,
                                      listdata: _presetList['tanggal'] ?? [],
                                      value: _rdstgllahir,
                                      onChanged: (String _val) {
                                        _rdstgllahir = _val;
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: _SelectField(
                                      labelText: 'Bulan',
                                      controller: _blnlahirctr,
                                      listdata: _presetList['bulan'] ?? [],
                                      value: _rdsblnlahir,
                                      onChanged: (String _val) {
                                        _rdsblnlahir = _val;
                                      }),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: _SelectField(
                                        labelText: 'Tahun',
                                        controller: _thnlahirctr,
                                        listdata: _presetList['tahun'] ?? [],
                                        value: _rdsthnlahir,
                                        onChanged: (String _val) {
                                          _rdsthnlahir = _val;
                                        }),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'No. Handphone',
                              controller: _mobilectr,
                              keyboardType: TextInputType.number,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Alamat',
                              controller: _addressctr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            StreamBuilder<int>(
                                stream: _provincesc.stream,
                                builder: (context, snapshot) {
                                  return _SelectField(
                                      labelText: 'Provinsi',
                                      controller: _provincectr,
                                      listdata: _presetList['province'] ??
                                          _belumadapilihan,
                                      value: _province,
                                      onChanged: (String _val) async {
                                        if (_province != _val) {
                                          _logging('_SelectField Provinsi');
                                          _province = _val;
                                          _presetList['kota_kab'] =
                                              _belumadapilihan;
                                          _presetList['kecamatan'] =
                                              _belumadapilihan;
                                          _presetList['kelurahan'] =
                                              _belumadapilihan;
                                          _cityctr.text = '';
                                          _subdistrictctr.text = '';
                                          _villagectr.text = '';
                                          _showLoadingDialog(context);
                                          await _getAddress(
                                              type: 'kota_kab',
                                              parenttype: 'province',
                                              parentname: _val);
                                          _citysc.sink.add(_unixtime());
                                          _subdistrictsc.sink.add(_unixtime());
                                          _villagesc.sink.add(_unixtime());
                                          _closeLoading(context);
                                        }

                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());
                                      });
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            StreamBuilder<int>(
                                stream: _citysc.stream,
                                builder: (context, snapshot) {
                                  return _SelectField(
                                      labelText: 'Kota/Kabupaten',
                                      controller: _cityctr,
                                      listdata: _presetList['kota_kab'] ??
                                          _belumadapilihan,
                                      value: _city,
                                      onChanged: (String _val) async {
                                        if (_city != _val) {
                                          _city = _val;
                                          _presetList['kecamatan'] =
                                              _belumadapilihan;
                                          _presetList['kelurahan'] =
                                              _belumadapilihan;
                                          _subdistrictctr.text = '';
                                          _villagectr.text = '';
                                          _showLoadingDialog(context);
                                          await _getAddress(
                                              type: 'kecamatan',
                                              parenttype: 'kota_kab',
                                              parentname: _val);
                                          _subdistrictsc.sink.add(_unixtime());
                                          _villagesc.sink.add(_unixtime());
                                          _closeLoading(context);
                                        }
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());
                                      });
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            StreamBuilder<int>(
                                stream: _subdistrictsc.stream,
                                builder: (context, snapshot) {
                                  return _SelectField(
                                      labelText: 'Kecamatan',
                                      controller: _subdistrictctr,
                                      listdata: _presetList['kecamatan'] ??
                                          _belumadapilihan,
                                      value: _subdistrict,
                                      onChanged: (String _val) async {
                                        if (_subdistrict != _val) {
                                          _subdistrict = _val;
                                          _presetList['kelurahan'] =
                                              _belumadapilihan;
                                          _villagectr.text = '';
                                          _showLoadingDialog(context);
                                          await _getAddress(
                                              type: 'kelurahan',
                                              parenttype: 'kecamatan',
                                              parentname: _val);
                                          _villagesc.sink.add(_unixtime());
                                          _closeLoading(context);
                                        }
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());
                                      });
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            StreamBuilder<int>(
                                stream: _villagesc.stream,
                                builder: (context, snapshot) {
                                  final _curval = '$_postalCode-$_village';
                                  return _SelectField(
                                      labelText: 'Kelurahan',
                                      controller: _villagectr,
                                      listdata: _presetList['kelurahan'] ??
                                          _belumadapilihan,
                                      value: _curval,
                                      onChanged: (String _val) async {
                                        if (_curval != _val) {
                                          try {
                                            RegExp _regExp = new RegExp(
                                              r"([0-9]+)\-(.*)",
                                              caseSensitive: true,
                                              multiLine: false,
                                            );
                                            final _res =
                                                _regExp.allMatches(_val);
                                            final _matches = _res.elementAt(0);
                                            final _curcode = _matches.group(1);
                                            _val = _curcode;
                                          } catch (e) {
                                            _logging(e);
                                          }
                                          _postalCode = _val;
                                          _postalCodectr.text = _val;
                                          _village = _villagectr.text;
                                          _logging('$_postalCode $_village');
                                        }
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());
                                      });
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Kode Post',
                              controller: _postalCodectr,
                              readOnly: true,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Status Pernikahan',
                                controller: _maritalStatusctr,
                                listdata: _presetList['maritalStatus'] ?? [],
                                value: _maritalStatus,
                                onChanged: (String _val) {
                                  _maritalStatus = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Pendidikan',
                                controller: _educationctr,
                                listdata: _presetList['education'] ?? [],
                                value: _education,
                                onChanged: (String _val) {
                                  _education = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Agama',
                                controller: _religionctr,
                                listdata: _presetList['religion'] ?? [],
                                value: _religion,
                                onChanged: (String _val) {
                                  _religion = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Jenis Usaha',
                                controller: _internetBasedctr,
                                listdata: _presetList['internetBased'] ?? [],
                                value: _internetBased,
                                onChanged: (String _val) {
                                  _internetBased = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Pengalaman Kerja',
                                controller: _workExperiencectr,
                                listdata: _presetList['workExperience'] ?? [],
                                value: _workExperience,
                                onChanged: (String _val) {
                                  _workExperience = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Pekerjaan',
                                controller: _jobctr,
                                listdata: _presetList['job'] ?? [],
                                value: _job,
                                onChanged: (String _val) {
                                  _job = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Jumlah Penghasilan (perbulan)',
                              controller: _salaryctr,
                              keyboardType: TextInputType.number,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Status Pekerjaan',
                                controller: _employmentTypectr,
                                listdata: _presetList['employmentType'] ?? [],
                                value: _employmentType,
                                onChanged: (String _val) {
                                  _employmentType = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Status Properti',
                                controller: _propertyStatusctr,
                                listdata: _presetList['propertyStatus'] ?? [],
                                value: _propertyStatus,
                                onChanged: (String _val) {
                                  _propertyStatus = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _SelectField(
                                labelText: 'Unit Bisnis',
                                controller: _businessUnitctr,
                                listdata: _presetList['businessUnit'] ?? [],
                                value: _businessUnit,
                                onChanged: (String _val) {
                                  _businessUnit = _val;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                }),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Nama Ibu Kandung',
                              controller: _motherMaidenNamectr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Nama Kontak Darurat',
                              controller: _emergencyNamectr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                              labelText: 'Hubungan',
                              controller: _emergencyRelationshipctr,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                                labelText: 'Nomor Kontak Darurat',
                                controller: _emergencyContactNoctr,
                                keyboardType: TextInputType.number),
                            SizedBox(
                              height: 20,
                            ),
                            _TextField(
                                labelText: 'Alamat Kontak Darurat',
                                controller: _emergencyAddressctr),
                            SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        _BackButton(
                          child: Icon(
                            Icons.arrow_back,
                            color: _boldGrey,
                            size: 20,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: _WithArrowButton(
                            label: 'Selanjutnya',
                            onTap: () async {
                              final _tempbirthDate =
                                  '$_rdstgllahir$_rdsblnlahir$_rdsthnlahir';
                              if (_namectr.text.trim() == '' ||
                                  _emailctr.text.trim() == '' ||
                                  _idCardctr.text.trim() == '' ||
                                  _gender == '' ||
                                  _birthPlacectr.text.trim() == '' ||
                                  _regExptgl.hasMatch(_tempbirthDate) ==
                                      false ||
                                  _mobilectr.text.trim() == '' ||
                                  _addressctr.text.trim() == '' ||
                                  _village == '' ||
                                  _subdistrict == '' ||
                                  _city == '' ||
                                  _province == '' ||
                                  _postalCode == '' ||
                                  _maritalStatus == '' ||
                                  _education == '' ||
                                  _religion == '' ||
                                  _internetBased == '' ||
                                  _salaryctr.text.trim() == '' ||
                                  _workExperience == '' ||
                                  _employmentType == '' ||
                                  _propertyStatus == '' ||
                                  _businessUnit == '' ||
                                  _motherMaidenNamectr.text.trim() == '' ||
                                  _emergencyNamectr.text.trim() == '' ||
                                  _emergencyRelationshipctr.text.trim() == '' ||
                                  _emergencyContactNoctr.text.trim() == '' ||
                                  _emergencyAddressctr.text.trim() == '') {
                                _alertWarning(
                                    context, 'Mohon lengkapi isian', 'Ulangi');
                                return;
                              }
                              if (_EmailValidator.validate(
                                      _emailctr.text.trim()) ==
                                  false) {
                                _alertWarning(
                                    context, 'Email tidak valid', 'Ulangi');
                                return;
                              }
                              _name = _namectr.text.trim();
                              _email = _emailctr.text.trim();
                              _idCard = _idCardctr.text.trim();
                              _birthPlace = _birthPlacectr.text.trim();
                              _birthDate = _tempbirthDate;
                              _mobile = _mobilectr.text.trim();
                              _address = _addressctr.text.trim();
                              _salary = _salaryctr.text.trim();
                              _motherMaidenName =
                                  _motherMaidenNamectr.text.trim();
                              _emergencyName = _emergencyNamectr.text.trim();
                              _emergencyRelationship =
                                  _emergencyRelationshipctr.text.trim();
                              _emergencyContactNo =
                                  _emergencyContactNoctr.text.trim();
                              _emergencyAddress =
                                  _emergencyAddressctr.text.trim();

                              final _respon = await Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          pageBuilder: (context, animation1,
                                                  animation2) =>
                                              _RegistrasiDuaFoto(),
                                          transitionDuration:
                                              Duration(seconds: 0)))
                                  as _ResponModel;
                              if (_respon == null) {
                                resyncAddress();
                              }
                              if (_respon != null && _respon.code != 201) {
                                Navigator.of(context).pop(_respon);
                              }
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                );
              }),
        ),
      ),
    );
  }
}
