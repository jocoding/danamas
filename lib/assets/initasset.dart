part of danamas;

Uint8List _imgneedregister;
Uint8List _imgstepsatu;
Uint8List _imgstepdua;
Uint8List _imgsteptiga;
Uint8List _imgstepempat;
Uint8List _imgalertwarning;
Uint8List _imgnoktp;
Uint8List _imgsuccessregister;
Uint8List _imgnoloan;
Uint8List _imguangpinjaman;
Uint8List _imgjatuhtempo;
Uint8List _imgdenda;
Uint8List _imgbank;
Uint8List _imgmap;
Uint8List _imgfailedregis;
Uint8List _iconwarning;
Uint8List _iconcheck;
Uint8List _iconcopy;
Uint8List _imgprofilepict;
Uint8List _imglocation;

void _initAsset() {
  _imgneedregister = base64Decode(_b64needregister);
  _imgstepsatu = base64Decode(_b64imgstepsatu);
  _imgstepdua = base64Decode(_b64imgstepdua);
  _imgsteptiga = base64Decode(_b64imgsteptiga);
  _imgstepempat = base64Decode(_b64imgstepempat);
  _imgalertwarning = base64Decode(_b64imgalertwarning);
  _imgnoktp = base64Decode(_b64imgnoktp);
  _imgsuccessregister = base64Decode(_b64successregister);
  _imgnoloan = base64Decode(_b64noloan);
  _imguangpinjaman = base64Decode(_b64uangpinjaman);
  _imgjatuhtempo = base64Decode(_b64jatuhtempo);
  _imgdenda = base64Decode(_b64denda);
  _imgbank = base64Decode(_b64bank);
  _imgmap = base64Decode(_b64imgmap);
  _imgfailedregis = base64Decode(_b64failedregis);
  _iconwarning = base64Decode(_b64iconwarning);
  _iconcheck = base64Decode(_b64iconcheck);
  _iconcopy = base64Decode(_b64iconcopy);
  _imgprofilepict = base64Decode(_b64imgprofilepict);
  _imglocation = base64Decode(_b64location);
}
