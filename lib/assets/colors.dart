part of danamas;

const _primaryRed = MaterialColor(0xFFDF0000, {
  50: Color.fromRGBO(87, 0, 0, .1),
  100: Color.fromRGBO(87, 0, 0, .2),
  200: Color.fromRGBO(87, 0, 0, .3),
  300: Color.fromRGBO(87, 0, 0, .4),
  400: Color.fromRGBO(87, 0, 0, .5),
  500: Color.fromRGBO(87, 0, 0, .6),
  600: Color.fromRGBO(87, 0, 0, .7),
  700: Color.fromRGBO(87, 0, 0, .8),
  800: Color.fromRGBO(87, 0, 0, .9),
  900: Color.fromRGBO(87, 0, 0, 1)
});

const _boldGrey = MaterialColor(0xFF9E9E9E, {
  50: Color.fromRGBO(62, 62, 62, .1),
  100: Color.fromRGBO(62, 62, 62, .2),
  200: Color.fromRGBO(62, 62, 62, .3),
  300: Color.fromRGBO(62, 62, 62, .4),
  400: Color.fromRGBO(62, 62, 62, .5),
  500: Color.fromRGBO(62, 62, 62, .6),
  600: Color.fromRGBO(62, 62, 62, .7),
  700: Color.fromRGBO(62, 62, 62, .8),
  800: Color.fromRGBO(62, 62, 62, .9),
  900: Color.fromRGBO(62, 62, 62, 1)
});

const _midGrey = MaterialColor(0xFF9EA4AC, {
  50: Color.fromRGBO(62, 64, 67, .1),
  100: Color.fromRGBO(62, 64, 67, .2),
  200: Color.fromRGBO(62, 64, 67, .3),
  300: Color.fromRGBO(62, 64, 67, .4),
  400: Color.fromRGBO(62, 64, 67, .5),
  500: Color.fromRGBO(62, 64, 67, .6),
  600: Color.fromRGBO(62, 64, 67, .7),
  700: Color.fromRGBO(62, 64, 67, .8),
  800: Color.fromRGBO(62, 64, 67, .9),
  900: Color.fromRGBO(62, 64, 67, 1)
});

const _lightGrey = MaterialColor(0xFFF8F8F8, {
  50: Color.fromRGBO(97, 97, 97, .1),
  100: Color.fromRGBO(97, 97, 97, .2),
  200: Color.fromRGBO(97, 97, 97, .3),
  300: Color.fromRGBO(97, 97, 97, .4),
  400: Color.fromRGBO(97, 97, 97, .5),
  500: Color.fromRGBO(97, 97, 97, .6),
  600: Color.fromRGBO(97, 97, 97, .7),
  700: Color.fromRGBO(97, 97, 97, .8),
  800: Color.fromRGBO(97, 97, 97, .9),
  900: Color.fromRGBO(97, 97, 97, 1)
});
