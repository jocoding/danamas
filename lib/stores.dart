part of danamas;

String _idReseller;
String _email;
String _password;
String _name;
String _idCard;
String _gender;
String _birthPlace;
String _birthDate;
String _mobile;
String _businessName;
String _businessDescription;
String _businessStartYear;
String _address;
String _village;
String _subdistrict;
String _city;
String _province;
String _postalCode;
String _businessAddress;
String _businessVillage;
String _businessSubdistrict;
String _businessCity;
String _businessProvince;
String _businessPostalCode;
String _createBy;

String _profileBinaryData64String;
String _profileName;
String _profileContentType;
String _binaryData64StringIdcard;
String _profileNameIdcard;
String _profileContentTypeIdcard;
String _lat;
String _lng;
String _maritalStatus;
String _education;
String _religion;
String _internetBased;
String _workExperience;
String _propertyStatus;
String _businessUnit;
String _job;
String _salary;
String _motherMaidenName;
String _employmentType;
String _emergencyName;
String _emergencyRelationship;
String _emergencyContactNo;
String _emergencyAddress;

String _apiurl;
String _signature;

Map<String, List<_PresetModel>> _presetList = {};

String _jsonlocalPresetlist =
    '[{"key":[{"code":"L","description":"Laki-laki"},{"code":"P","description":"Perempuan"}],"name":"gender"},{"key":[{"code":"01","description":"Januari"},{"code":"02","description":"Februari"},{"code":"03","description":"Maret"},{"code":"04","description":"April"},{"code":"05","description":"Mei"},{"code":"06","description":"Juni"},{"code":"07","description":"Juli"},{"code":"08","description":"Agustus"},{"code":"09","description":"September"},{"code":"10","description":"Oktober"},{"code":"11","description":"November"},{"code":"12","description":"Desember"}],"name":"bulan"},{"key":[{"code":"Contract","description":"Karyawan Kontrak"},{"code":"Internship/Honorary","description":"Karyawan Honorer"},{"code":"Resigned Employee","description":"Mengundurkan Diri"},{"code":"Mutation Employee","description":"Karyawan Mutasi"},{"code":"Retired Employee","description":"Pensiunan"},{"code":"Permanent","description":"Karyawan Tetap"}],"name":"employmentType"}]';

int _isLoading = -1;

Map<String, dynamic> _mapdata = {};
