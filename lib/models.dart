part of danamas;

class _PresetModel {
  final String code;
  final String description;

  _PresetModel({this.code, this.description});

  static _PresetModel fromJson(Map<String, dynamic> json) {
    return _PresetModel(code: json['code'], description: json['description']);
  }
}

class _ResponModel {
  final num code;
  final bool status;
  final String message;
  final dynamic data;
  final dynamic meta;

  _ResponModel({this.code, this.status, this.message, this.data, this.meta});

  static _ResponModel fromJson(dynamic json) {
    // final json = jsonDecode(str) as Map<String, dynamic>;
    return _ResponModel(
        code: json['code'],
        status: json['status'],
        message: json['message'],
        data: json['data'],
        meta: json['meta']);
  }
}

class _PinjamanModel {
  final DateTime createdAt;
  final bool isPaid;
  final bool isDisbursement;
  final num totalPinjaman;
  final DateTime jatuhTempo;
  final DateTime pelunasan;
  final num denda;
  final String namaBank;
  final String nomorRekening;
  final String loanCode;
  _PinjamanModel(
      {this.createdAt,
      this.isPaid,
      this.isDisbursement,
      this.totalPinjaman,
      this.jatuhTempo,
      this.pelunasan,
      this.denda,
      this.namaBank,
      this.nomorRekening,
      this.loanCode});

  static _PinjamanModel fromJson(Map<String, dynamic> json) {
    final detail = json['loan_application_detail'] ?? {};
    final loanuserpayments = detail['loan_user_payments'] ?? [];
    String pelunasan = '';
    for (int i = 0; i < loanuserpayments.length; i++) {
      final curdat = loanuserpayments[i];
      if (curdat['outstandingAmount'] != null) {
        final outstandingAmount = '${curdat['outstandingAmount']}';
        try {
          final sisa = double.parse(outstandingAmount);
          if (sisa == 0) pelunasan = curdat['createdAt'] ?? '';
        } catch (e) {
          _logging(e);
        }
      }
    }
    return _PinjamanModel(
        createdAt: detail['createdAt'] != null
            ? DateTime.parse(json['createdAt'])
            : null,
        isPaid: detail['isPaid'],
        isDisbursement: detail['isDisbursement'],
        totalPinjaman: detail['loanAmount'] ?? 0,
        jatuhTempo: detail['jatuhTempo'] != null
            ? DateTime.parse(detail['jatuhTempo'])
            : null,
        pelunasan: pelunasan != '' ? DateTime.parse(pelunasan) : DateTime.now(),
        // denda: detail['denda'] ?? 0,
        namaBank: 'Virtual Account',
        nomorRekening: detail['virtualAccount'] ?? '',
        loanCode: detail['loanCode']);
  }
}

// {
//     "namaBank": "Virtual Account BCA",
//     "nomorRekening": "0338778738343983"
//   }
class _InfoPembayaranModel {
  final String namaBank;
  final String nomorRekening;

  _InfoPembayaranModel({this.namaBank, this.nomorRekening});

  static _InfoPembayaranModel fromJson(Map<String, dynamic> json) {
    return _InfoPembayaranModel(
        namaBank: json['namaBank'], nomorRekening: json['nomorRekening']);
  }
}
